/*! @mainpage Parcial electrónica programable 2C2020
 *
 * \section genDesc General Description
 *
 * Este es el firmware correspondiente a la resoluión del parcial de electrónica programable del 2C2020
 *
 *
 * <a href="https://photos.app.goo.gl/WqBcdH8JyCZcu5Ys9">Operation Example</a>
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 2/11/2020 | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
#define PARCIAL_H
#ifndef PARCIAL_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _MEDIDOR_DISTANCIA_H */

