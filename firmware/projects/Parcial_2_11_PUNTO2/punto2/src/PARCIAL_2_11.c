/*! @mainpage Parcial 2_11 ejercicio 2
 *
 * \section genDesc General Description
 *
 * Este es el firmware correspondiente al ejercicio 2 del parcial de electrónica programable del día 2/11/2020.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 2/11/2020  | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
/*CONSIGNA PUNTO 2: Digitalice la señal del conversor que mide el voltaje generado por el punto medio
 * de un potenciómetro conectado a la entrada CH2 de la EDU-CIAA a una frecuencia de muestreo de 10Hz.
 * Tome el valor promedio cada 1 segundo y envíelo por el puerto serie a la pc en un formato de caracteres
 * legible por consola (uno por renglón). Se debe usar interrupciones y timer.
 * (Conecte los extremos del potenciómetro a VDDA y GNDA).*/
/*==================[inclusions]=============================================*/
#include "../../Parcial_2_11_PUNTO2/inc/PARCIAL_2_11.h"       /* <= own header */

#include "systemclock.h"
#include "bool.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "led.h"
/*==================[macros and definitions]=================================*/
/*Se definen los macros que se utilizaran en la resolución de la consigna*/

/*velocidad de envío y recepción de datos del puerto serie*/
#define BAUDIOS 115200

/*período de muestreo de la señal*/
#define PERIODO_AD 100 // 10Hz corresponde a un período de 100 ms.

/*cantidad de muestras por segundo para procesamiento*/
#define CANT_MUESTRAS 10 //10 por segundo.

/*==================[internal data definition]===============================*/
uint16_t promedio;

/*variable de estado para definir si la conversión se realizó*/
uint8_t conversion=false;

/*señal que se lee desde el conversor*/
uint16_t senial;

/*==================[internal functions declaration]=========================*/
/** @fn void adq_senial(void)
 * @brief Disparar la conversión de la señal a una frecuencia de 10 Hz.
 * @param[in] void.
 * @return null
 */
void adq_senial(void);
/** @fn void func_ADC(void)
 * @brief Conversor de una señal analógica a una digital para su posterior procesamiento.
 * @param[in] void.
 * @return null
 */
void func_ADC (void);
/*==================[external data definition]===============================*/
/*se definen los parámetros para la utilización del timer, del puerto serie y del conversor
 * analógico digital
 */
timer_config my_timer = { TIMER_A, PERIODO_AD, &adq_senial }; // fm=10Hz
/*no se realizan lecturas desde el puerto serie por lo que el tercer parámetro del struct se declara como NO_INT*/
serial_config my_serial_port = {SERIAL_PORT_PC, BAUDIOS, NO_INT};
analog_input_config my_ADC = { CH2, AINPUTS_SINGLE_READ, &func_ADC };
/*==================[external functions definition]==========================*/
void adq_senial (){
	AnalogStartConvertion();
}

void func_ADC(){
	{
	/*función del ADC para adquirir una señal*/
	AnalogInputRead(my_ADC.input, &senial);

	/*cambio el estado de la variable*/
	conversion = true;
	}
}
void InitSystem(){
	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	/* inicialización de los timers*/
	TimerInit(&my_timer);
	TimerStart(my_timer.timer);
	/* inicialización del puerto serie*/
	UartInit(&my_serial_port);
	/* inicialización del conversor*/
	AnalogInputInit(&my_ADC);
}
int main(void) {
	InitSystem();

	uint16_t acumulador = 0;
	uint8_t i = 0;
	uint8_t salto_de_linea[] = "\r\n";
	while (1) {
		if (conversion) {
			if (i < 10) {
				acumulador += senial;
				i++;

			} //end del if de la cantidad de muestras

			else {
				promedio = acumulador / CANT_MUESTRAS;
				/*se envían los datos a través de la UART*/
				UartSendString(my_serial_port.port, UartItoa(promedio, 10));
				UartSendString(my_serial_port.port, salto_de_linea);
				acumulador = senial; //para no perder el siguiente dato correspondiente a la onceava muestra
				i = 0;
			}
			conversion = false;
		} //end del if de la conversion

	} //end del while
} //end del main

/** @} doxygen end group definition */
/*==================[end of file]============================================*/

