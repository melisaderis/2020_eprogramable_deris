/*! @mainpage Distance meter
 *
 * \section genDesc General Description
 *
 * This application makes the leds blink according to the distance that distance sensor is measuring.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 20/09/2020 | Document creation		                         |
 * | 22/09/2020	| Document modification. Version 1.0 			 |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
/*
 *
 * CONSIGNA:
 * 1) Mostrar distancia medida utilizando los leds de la siguiente manera:
a) Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
b) Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y
LED_1.
c) Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul),
LED_1 y LED_2
d) Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1,
LED_2 y LED_3.
2) Usar TEC1 para activar y detener la medición.
3) Usar TEC2 para mantener el resultado (“HOLD”).
 *
 */

/*==================[inclusions]=============================================*/
#include "../../3_medidor_distancia/inc/medidor_distancia.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
bool encender = false; // variable de estado para controlar el encendido y el apagad de la medición
bool hold = false; // variable de estado para controlar cuando la medición se detiene

/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/

/* 		Interrupción para cuando se presione la tecla 1.
 *		Modifica la variable de estado encender.
 */
void Tecla1() {
	if (encender == true) {
		encender = false;
	} else {
		encender = true;
	}

}

/* 		Interrupción para cuando se presione la tecla 2.
 *		Modifica la variable de estado hold.
 */
void Tecla2() {
	if (hold == true) {
		hold = false;
	} else {
		hold = true;
	}

}


int main(void)
{

	uint16_t distancia;
	uint16_t distancia_hold;

	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	/*T_FIL2 ->pin echo, T_FIL3 -> pin trigger */
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	/* inicialización de las interrupciones*/
	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	while (1){

		if (encender == true) {
			distancia = HcSr04ReadDistanceInCentimeters();
			if (hold == false) {
				distancia_hold = distancia;
				if (distancia_hold > 0 && distancia_hold <= 10) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
					LedOn(LED_RGB_B);
					LedOff(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				if (distancia_hold > 10 && distancia_hold <= 20) {
					//Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y LED_1.
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOff(LED_2);
					LedOff(LED_3);
				}
				if (distancia_hold > 20 && distancia_hold <= 30) {
				// Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED_1 y LED_2
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOff(LED_3);
				}
				if (distancia_hold > 30) {
				//Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
					LedOn(LED_RGB_B);
					LedOn(LED_1);
					LedOn(LED_2);
					LedOn(LED_3);
				}

			}//end if hold=false

		}//end if de encender=true

		else {
			LedOff(LED_RGB_B);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
		}


		DelayMs(250);
	} //end del while

	return 0;
} //end del main

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
