/*! @mainpage Digital Osciloscope
 *
 * \section genDesc General Description
 *
 * This application simulates a digital oscilloscope by an analog-digital converter.
 *
 * <a href="https://photos.app.goo.gl/85sqsrrPu6336D2y6">Operation Example</a>
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/10/2020 | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
#define _SIMULADOR_OSC_H
#ifndef _SIMULADOR_OSC_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _SIMULADOR_OSC_H */

