/*! @mainpage Distance meter
 *
 * \section genDesc General Description
 *
 * This application simulates a digital oscilloscope by an analog-digital converter.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/10/2020  | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/simulador_osc.h"       /* <= own header */

#include "systemclock.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"

/*==================[macros and definitions]=================================*/
#define BAUDIOS 115200
#define ADC_PERIOD 2 //frecuencia de muestreo 500hz
#define DAC_PERIOD 4 // para respetar que el ECG tenga 1seg.
#define BUFFER_SIZE 231


/*==================[internal data definition]===============================*/
uint16_t dato;
uint8_t timer_dac =false;
uint8_t conversion= false;
const char ecg[BUFFER_SIZE] = { 76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91, 99, 105, 101, 96, 102,
		106, 101, 96, 100, 107, 101, 94, 100, 104, 100, 91, 99, 103, 98, 91, 96,
		105, 95, 88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80, 83, 92,
		86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82, 79, 69, 80, 82, 81, 70,
		75, 81, 77, 74, 79, 83, 82, 72, 80, 87, 79, 76, 85, 95, 87, 81, 88, 93,
		88, 84, 87, 94, 86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144, 180, 210, 236,
		253, 227, 171, 99, 49, 34, 29, 43, 69, 89, 89, 90, 98, 107, 104, 98,
		104, 110, 102, 98, 103, 111, 101, 94, 103, 108, 102, 95, 97, 106, 100,
		92, 101, 103, 100, 94, 98, 103, 96, 90, 98, 103, 97, 90, 99, 104, 95,
		90, 99, 104, 100, 93, 100, 106, 101, 93, 101, 105, 103, 96, 105, 112,
		105, 99, 103, 108, 99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88,
		77, 69, 75, 79, 74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77,
		77, 76, 76, };
/*==================[internal functions declaration]=========================*/
void timer_ADC_read(void);
void timer_DAC_read(void);
void uart_read(void);
void func_ADC (void);
/*==================[external data definition]===============================*/
timer_config my_timer_ADC = { TIMER_A, ADC_PERIOD, &timer_ADC_read }; // fm=500Hz
timer_config my_timer_DAC = { TIMER_B, DAC_PERIOD, &timer_DAC_read };
serial_config my_serial_port = {SERIAL_PORT_PC, BAUDIOS, NO_INT};
analog_input_config my_ADC = { CH1, AINPUTS_SINGLE_READ, &func_ADC };

/*==================[external functions definition]==========================*/
void timer_ADC_read() {
	AnalogStartConvertion();
} //función que configura la conversión del ADC cada 2ms

void timer_DAC_read(){
	timer_dac = true;
} //función que cambia la bandera correspondiente al timer del conversor DA para poder escribir el dato cada 4ms.

/*void uart_read() {
	uint8_t dato_leido;
	UartReadByte(my_serial_port.port, &dato_leido);}*/


void func_ADC(void) {
	AnalogInputRead(my_ADC.input, &dato);
	conversion = true;
}

void InitSystem(){
	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	/* inicialización de los timers*/
	TimerInit(&my_timer_ADC);
	TimerInit(&my_timer_DAC);
	TimerStart(my_timer_ADC.timer);
	TimerStart(my_timer_DAC.timer);
	/* inicialización del puerto serie*/
	UartInit(&my_serial_port);
	/* inicialización del conversor*/
	AnalogInputInit(&my_ADC);
	AnalogOutputInit();
}
int main(void) {
	uint8_t i=0;
	InitSystem();
	while (1) {
		if (conversion == true) {
			// 		Datos enviados por puerto serie
			UartSendString(my_serial_port.port, UartItoa(dato, 10));
			UartSendString(my_serial_port.port, "\r");
			conversion= false;
		}

		if (timer_dac == true) {
			AnalogOutputWrite(ecg[i]);

			if (i < BUFFER_SIZE) {
				i++;
			}
			else
				i = 0;
		}


		timer_dac = false;

	} //end del while

} //end del main

/** @} doxygen end group definition */
/*==================[end of file]============================================*/

