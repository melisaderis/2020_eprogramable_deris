/*! @mainpage Distance meter
 *
 * \section genDesc General Description
 *
 * This application makes the distance sensor measures from the use of a serial port.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 4/10/2020  | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
/*
 *
 * Distancia mínima: 2 cm
 * Distancia máxima: 130 cm
 * linealidad : >10%
 * Resolución : 1 cm
 * Refresco de medición: 1 s
 * Velocidad de Transferencia de datos: 115.200 baudios.
 * Formato de datos: 3 dígitos ascii + 1 carácter espacio + dos caracteres para la unidad (cm) + cambio de línea “ \r\n”
 * Con las teclas “O” y “H”, replicar la funcionalidad de las teclas 1 y 2 de la EDU-CIAA
 *
 */

/*==================[inclusions]=============================================*/
#include "../../P3_6_medidor_distancia_uart/inc/medidor_distancia_uart.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "hc_sr4.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define BAUDIOS 115200
bool encender = false; // variable de estado para controlar el encendido y el apagad de la medición
bool hold = false; // variable de estado para controlar cuando la medición se detiene
bool timer_var = false; //variable de estado para controlar el funcionamiento del timer
uint16_t distancia;
uint16_t distancia_hold;
uint8_t dato_recibido;
/*==================[internal data definition]===============================*/
void timer_read(void);
void uart_read(void);

/* 		Interrupción para cuando se presione la tecla 1.
 *		Modifica la variable de estado encender.
 */
/*void Tecla1() {
	encender = !encender;
}
*/
/* 		Interrupción para cuando se presione la tecla 2.
 *		Modifica la variable de estado hold.
 */
/*void Tecla2() {
	hold = !hold;
}
*/
/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/
timer_config my_timer = { TIMER_A, 1000, &timer_read };
serial_config my_serial_port = {SERIAL_PORT_PC, BAUDIOS, &uart_read};

/*==================[external functions definition]==========================*/
void timer_read() {
	timer_var = true;
}

void uart_read() {
	/* 		Interrupción para cuando se lea el dato O.
	 *		Modifica la variable de estado encender.
	 */
	UartReadByte(my_serial_port.port, &dato_recibido);
	if (dato_recibido == 'O') {
		encender = !encender;
	}
	/* 		Interrupción para cuando se lea el dato h.
	 *		Modifica la variable de estado encender.
	 */
	if (dato_recibido == 'H') {
		hold = !hold;
	}

}
void InitSystem(){
	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	/*T_FIL2 ->pin echo, T_FIL3 -> pin trigger */
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);

	/* inicialización de las interrupciones*/
	/*SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);*/

	/* inicialización del timer*/
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&my_serial_port);
}
int main(void) {
	InitSystem();
	uint8_t caract[] = "cm \n\r";
	while (1) {
		if (timer_var == true) {
			if (encender == true) {
				distancia = HcSr04ReadDistanceInCentimeters();
				if (hold == false) {
					distancia_hold = distancia;
					if (distancia_hold > 0 && distancia_hold <= 10) { //Si la distancia está entre 0 y 10 cm, encender el LED_RGB_B (Azul).
						LedOn(LED_RGB_B);
						LedOff(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 10 && distancia_hold <= 20) {
						//Si la distancia está entre 10 y 20 cm, encender el LED_RGB_B (Azul) y LED_1.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOff(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 20 && distancia_hold <= 30) {
						// Si la distancia está entre 20 y 30 cm, encender el LED_RGB_B (Azul), LED_1 y LED_2
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOff(LED_3);
					}
					if (distancia_hold > 30) {
						//Si la distancia es mayor a 30 cm, encender el LED_RGB_B (Azul), LED_1, LED_2 y LED_3.
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_3);
					}

				} //end if hold=false

				UartSendString(my_serial_port.port,UartItoa(distancia_hold,10));
				UartSendString(my_serial_port.port, caract);
			} //end if de encender=true

			else {
				LedOff(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			timer_var = false;

		} // end del if del timer_var

	} //end del while

	return 0;
} //end del main

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
