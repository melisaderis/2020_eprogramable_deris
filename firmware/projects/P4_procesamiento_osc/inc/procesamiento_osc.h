/*! @mainpage Distance meter
 *
 * \section genDesc General Description
 *
 * This application makes the distance that distance sensor is measuring from the use of a serial port.
 *
 * <a href="https://photos.app.goo.gl/WqBcdH8JyCZcu5Ys9">Operation Example</a>
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 4/10/2020 | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
#define _MEDIDOR_DISTANCIA_H
#ifndef _MEDIDOS_DISTANCIA_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _MEDIDOR_DISTANCIA_H */

