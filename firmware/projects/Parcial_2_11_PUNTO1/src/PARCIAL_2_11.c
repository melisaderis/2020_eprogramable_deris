/*! @mainpage Parcial 2_11 ejercicio 1
 *
 * \section genDesc General Description
 *
 * Este es el firmware correspondiente al parcial de electrónica programable del día 2/11/2020.
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 2/11/2020  | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
/*CONSIGNA PUNTO 1: Diseñar e implementar el firmware de un sistema de medición de volumen.
 * Suponga que tiene un vaso de 6 cm de diámetro y 10 cm de alto,
 * se coloca el sensor de distancia en la parte superior como muestra en la figura:
 * Calcule a partir de la distancia medida a la superficie del agua la cantidad de la misma que contiene el vaso.
 * Envíe este dato 6 veces por segundo por la UART a la PC en el siguiente formato: xx + “ cm3” (uno por renglón).
 * Utilice interrupciones de timer. (Para el cálculo de volumen aproxime pi a 3,14).*/
/*==================[inclusions]=============================================*/
#include "../../Parcial_2_11_PUNTO1/inc/PARCIAL_2_11.h"       /* <= own header */

#include "systemclock.h"
#include "bool.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "hc_sr4.h" /*header correspondiente al driver del sensor de distancia*/
/*==================[macros and definitions]=================================*/
/*Se definen los macros correspondientes a las variables que se van a utilizar para la resolución de la aplicación*/
#define DIAMETRO 6
#define ALTO 10
#define PI 3.14
/*la frecuencia a la cual se tomarán los datos será de 6Hz*/
#define PERIODO 170 //[ms]

/*se define la velocidad del puerto serie*/
#define BAUDIOS 115200
/*==================[internal data definition]===============================*/
/*valor leído por el sensor*/
uint16_t distancia;

uint16_t volumen;
/*variable de estado para controlar el funcionamiento del timer*/
uint8_t timer_st = false;

/*==================[internal functions declaration]=========================*/
/** @fn void send_data(void)
 * @brief Send data from serial port
 * @param[in] void.
 * @return null
 */
void send_data(void);
/** @fn uint16_t calculo_volumen(uint16_t valor_medido)
 * @brief Calcular el volumen del agua dentro del vaso
 * @param[in] valor obtenido a partir de la medición con el sensor.
 * @return
 */
uint16_t calculo_volumen(uint16_t valor_medido);
/*==================[external data definition]===============================*/
/*definición de los parámetros correspondientes a las interrupciones que se utilizarán*/

/*declaración del timer, el cual tendrá un periodo correspondiente a 6 veces por segundo*/
timer_config my_timer = { TIMER_A, PERIODO, &send_data };

/*declaración del puerto serie a través del cual se enviarán los datos, el cual no recibirá datos por lo tanto
 * en lugar de una subrutina se escribe el parámetro NO_INT*/

serial_config my_serial_port = {SERIAL_PORT_PC, BAUDIOS, NO_INT};
/*==================[external functions definition]==========================*/
void send_data(){
	timer_st = true; /*se cambia el valor de la variable cada vez que el timer cumple con el período establecido*/
}

uint16_t calculo_volumen(uint16_t valor_medido){
	uint16_t volumen;
	uint16_t vol_total = ALTO * ((DIAMETRO*DIAMETRO)/4) * PI; //volumen total que el vaso puede albergar

	/*se calcula el volumen que no tiene agua a partir de la medición realizada con el sensor y se le resta este valor
	 * al volumen total para obtener la cantidad de agua */
	volumen = vol_total - (((DIAMETRO*DIAMETRO)/4) * PI * valor_medido);
	return volumen;
}

void InitSystem(){
	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	/*T_FIL2 ->pin echo, T_FIL3 -> pin trigger */
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	/* inicialización de los timers*/
	TimerInit(&my_timer);
	TimerStart(my_timer.timer);
	/* inicialización del puerto serie*/
	UartInit(&my_serial_port);
}
int main(void) {
	InitSystem();

	/*se definen los strings para enviar por el puerto serie*/
	uint8_t unidades[] = " cm3 \r\n ";
	uint8_t salto_de_linea[] = "\r\n";
	while (1) {
		if (timer_st){
			/*se lee la distancia con la función definida en el driver del sensor*/
			distancia = HcSr04ReadDistanceInCentimeters();
			/*se llama a la función de calculo de volumen*/
			volumen= calculo_volumen(distancia);

			/*se envían los datos a través de la UART*/
			UartSendString(my_serial_port.port, UartItoa(volumen, 10));
			UartSendString(my_serial_port.port, unidades);
			UartSendString(my_serial_port.port, salto_de_linea);
		}
		timer_st =false;
	} //end del while
} //end del main

/** @} doxygen end group definition */
/*==================[end of file]============================================*/

