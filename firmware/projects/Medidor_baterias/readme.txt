﻿Medidor de baterias. 

Mide el voltaje de la bateria y determina si está cargada o no y también calcula su capacidad. 
Estos valores se sacan por un display LCD y a la vez se prenden LEDS:
BLANCO -> si la batería está sobrecargada. 
VERDE -> si la batería está cargada
AMARILLO -> si la batería tiene media carga.
ROJO -> si está con batería baja
AZUL ->si la batería debe ser recargada. 

Para demostrar su funcionamiento se conectará un potenciometro para simular la carga y la descarga. 
Extremo pote -> VDDA
Pto Medio -> CH1
Extremo pote -> GNDA


Conexión LCD 
PIN LCD -> PIN CIAA
K -> GND
A -> R=270 -> 5V
D7 -> LCD4
D6 -> LCD3
D5 -> LCD2
D4 -> LCD1
E -> LCD_EN
RS -> LCD_RS
RW -> GND
V0 -> GND
VDD -> 5V
VSS -> GND