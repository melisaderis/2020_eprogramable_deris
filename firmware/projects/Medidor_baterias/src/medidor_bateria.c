/*! @mainpage Medidor de baterias
 *
 * \section genDesc General Description
 *
 * Esta aplicación simula un medidor de la carga y de la capacidad de una batería de litio de 4.2V.
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 4/11/2020  | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */

/*==================[inclusions]=============================================*/
#include "medidor_bateria.h"       /* <= own header */
#include "systemclock.h"
#include "switch.h"
#include "bool.h"
#include "delay.h"
#include "gpio.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "led.h"
#include "delay.h"
#include "lcd.h"

/*==================[macros and definitions]=================================*/
#define BAUDIOS 115200
#define PERIODO 1000
#define GANANCIA_DIVISOR 2 /*ganancia divisor resistivo en la proto para que los valores no excedan los 3.3*/
#define VOLTAJE_MAX 4.3 /*voltaje máximo que puede almacenar una pila de Li*/
/*porcentajes del voltaje máximo de la pila para determinar la capacidad de la misma*/
#define VOLTAJE_OK 4
#define VOLTAJE_MIN 2.7
#define VOLTAJE_MED 3.2
/*valores obtenidos a partir de interpolar los valores de voltaje en la curva V/capcidad de una pila de litio*/
#define CTE_MV 1000
#define CTE_INTERPOLACION 0.017
#define CTE_PORCENTAJE 100


typedef enum {CARGA, CAPACIDAD} pila_t;
/*==================[internal data definition]===============================*/
uint16_t voltaje_recibido;
float voltaje;
uint16_t capacidad;
uint8_t conversion;
pila_t pila;
/*==================[internal functions declaration]=========================*/

/*interrupciones correspondientes a las teclas*/
/** @brief Funcion cuando se presiona la tecla 1.
 * @param[in] void.
 * @return null
 */
void Tecla1(void);
/** @brief Funcion cuando se presiona la tecla 2.
 * @param[in] void.
 * @return null
 */
void Tecla2(void);
/** @brief Funcion del timer.
 * @param[in] void.
 * @return null
 */
void timer_ADC(void);
/** @brief Funcion para tomar el valor que entrega el conversor AD.
 * @param[in] void.
 * @return null
 */
void medidor_voltaje(void);
/** @brief Funcion de calculo de voltaje y capacidad.
 * @param[in] void.
 * @return null
 */
void calculo_voltaje_y_capacidad(void);
/** @brief Funcion para encender los leds de acuerdo al porcentaje de bateria.
 * @param[in] void.
 * @return null
 */
void encender_leds(void);
/** @brief Funcion para mostrar por puerto serie y LCD la carga de la pila.
 * @param[in] void.
 * @return null
 */
void visualizador_carga(void);
/** @brief Funcion para mostrar por puerto serie y LCD la capacidad de la pila.
 * @param[in] void.
 * @return null
 */
void visualizador_capacidad(void);
/*==================[external data definition]===============================*/
timer_config my_timer = { TIMER_A, PERIODO, &timer_ADC };
serial_config my_serial_port = {SERIAL_PORT_PC, BAUDIOS, NO_INT};
analog_input_config my_ADC = { CH1, AINPUTS_SINGLE_READ, &medidor_voltaje};

/*==================[external functions definition]==========================*/
void timer_ADC() {
	AnalogStartConvertion();
}

/*void uart_read() {
	uint8_t dato_leido;
	UartReadByte(my_serial_port.port, &dato_leido);}*/


void medidor_voltaje(void) {
	AnalogInputRead(my_ADC.input, &voltaje_recibido);
	conversion = true;
}


void Tecla1() {
	pila = CARGA;
}

void Tecla2() {
	pila= CAPACIDAD;
}

void calculo_voltaje_y_capacidad(){
	voltaje = (voltaje_recibido*(3.3/1023))*GANANCIA_DIVISOR;
	capacidad = ((voltaje-VOLTAJE_MAX)/CTE_INTERPOLACION)+CTE_PORCENTAJE;
}
void encender_leds(){
	float umbral_sobrecarga = VOLTAJE_MAX;
	float umbral_max = VOLTAJE_OK;
	float umbral_med = VOLTAJE_MED;
	float umbral_min = VOLTAJE_MIN;
	if(voltaje > umbral_sobrecarga){
		LedOn(LED_RGB_B);
		LedOn(LED_RGB_R);
		LedOn(LED_RGB_G);
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);
		UartSendString(my_serial_port.port,
				"La bateria esta sobrecargada");
		UartSendString(my_serial_port.port, "\r\n");
		lcdClear();
		lcdCursorSet( LCD_CURSOR_OFF );
		lcdGoToXY( 0, 0 );
		lcdSendString("Sobrecarga");

	}
	else if (voltaje >= umbral_max) {
			LedsMask(LED_RGB_G);
			UartSendString(my_serial_port.port,
					"La bateria esta cargada");
			UartSendString(my_serial_port.port, "\r\n");
			lcdClear();
			lcdCursorSet( LCD_CURSOR_OFF );
			lcdGoToXY( 0, 0 );
			lcdSendString("Bateria Cargada");}
	else if (voltaje >= umbral_med) {
			LedsMask(LED_2);
			UartSendString(my_serial_port.port,
					"La bateria esta a media carga");
			UartSendString(my_serial_port.port, "\r\n");
			lcdClear();
			lcdCursorSet( LCD_CURSOR_OFF );
			lcdGoToXY( 0, 0 );
			lcdSendString("Media carga");
		} //end if umbral_med
		else if (voltaje >= umbral_min) {
			LedsMask(LED_RGB_R);
			UartSendString(my_serial_port.port,
					"La bateria esta con su carga minima");
			UartSendString(my_serial_port.port, "\r\n");
			lcdClear();
			lcdCursorSet( LCD_CURSOR_OFF );
			lcdGoToXY( 0, 0 );
			lcdSendString("Carga minima");
		} //end del if de umbral min
		else {
			LedsMask(LED_RGB_B);
			UartSendString(my_serial_port.port,
					"Por favor recargue o cambie su bateria");
			UartSendString(my_serial_port.port, "\r\n");
			lcdClear();
			lcdCursorSet( LCD_CURSOR_OFF );
			lcdGoToXY( 0, 0 );
			lcdSendString("Recargue bat");
		}
}
void visualizador_carga(){

		UartSendString(my_serial_port.port,
				"El voltaje de la bateria es:");
		UartSendString(my_serial_port.port, UartItoa((voltaje*CTE_MV), 10));
		UartSendString(my_serial_port.port, "mV");
		UartSendString(my_serial_port.port, "\r\n");
		lcdCursorSet( LCD_CURSOR_OFF );
		lcdGoToXY( 0, 1 );
		lcdSendString("Volt:");
		lcdSendString(UartItoa((voltaje*CTE_MV), 10));
		lcdSendString("mV");
}

void visualizador_capacidad(){
	UartSendString(my_serial_port.port,
			"La capacidad de la bateria es:");
	UartSendString(my_serial_port.port, UartItoa((capacidad), 10));
	UartSendString(my_serial_port.port, "%");
	UartSendString(my_serial_port.port, "\r\n");
	lcdCursorSet( LCD_CURSOR_OFF );
	lcdGoToXY( 0, 1 );
	lcdSendString("Capacidad:");
	lcdSendString(UartItoa(capacidad, 10));
	lcdSendString("%");
}
void InitSystem(){
	/*inicialización de los periféricos y dispostivos*/
	SystemClockInit();
	SwitchesInit();
	LedsInit();
	/* inicialización de las interrupciones*/
	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	/* inicialización del timers*/
	TimerInit(&my_timer);
	TimerStart(my_timer.timer);
	/* inicialización del puerto serie*/
	UartInit(&my_serial_port);
	/* inicialización del conversor*/
	AnalogInputInit(&my_ADC);
	lcdInit(16, 2, 5, 8);//configura la correlación de puertos con pines
	lcdCursorSet( LCD_CURSOR_OFF ); //Apaga el cursor
	lcdClear();
}
int main(void) {
	InitSystem();
	while (1) {
		if (conversion) {
			calculo_voltaje_y_capacidad();
			encender_leds();
			switch (pila) {
			case CARGA:
				visualizador_carga();
				break;
			case CAPACIDAD:
				visualizador_capacidad();
				break;
			}

			conversion = false;
		}
	} //end del while

} //end del main

/** @} doxygen end group definition */
/*==================[end of file]============================================*/

