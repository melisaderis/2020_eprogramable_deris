/*! @mainpage Medidor de bateria
 *
 * \section genDesc General Description
 *
 *
 *
 * <a href="https://photos.app.goo.gl/53iVSFHP8ioMqGfL8">Operation Example</a>
 * \section hardConn Hardware Connection
 *

 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 4/10/2020 | Document creation		                         |
 *
 * @author Deris Melisa
 * @note FIUNER - 2do cuatrimestre 2020
 *
 */
#define _MEDIDOR_DISTANCIA_H
#ifndef _MEDIDOS_DISTANCIA_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _MEDIDOR_DISTANCIA_H */

