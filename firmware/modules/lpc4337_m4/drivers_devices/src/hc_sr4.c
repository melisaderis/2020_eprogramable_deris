/* * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autor/es:
 * Deris, Melisa
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "delay.h"
#include "bool.h"
#include "gpio.h"
#include "../../drivers_devices/inc/hc_sr4.h" //own header
/*==================[macros and definitions]=================================*/

#define PULSO_TRIGGER 10
#define PULSO_ECHO 1

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/
//Variables de estado de los pines
bool HIGH = true;
bool LOW = false;

uint16_t distance_cm = 0;
uint16_t distance_inches = 0;

//Acumulador para conteo
uint32_t cont_cm = 0;
uint32_t cont_inches = 0;

//Echo y trigger inicializados con HcSr04Init
gpio_t pin_echo;
gpio_t pin_trigger;
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	pin_echo = echo;
	pin_trigger = trigger;
// se configuran los puertos como entrada y salida según corresponde a la consigna
	GPIOInit(pin_echo, GPIO_INPUT);
	GPIOInit(pin_trigger, GPIO_OUTPUT);

	return true;
}

uint16_t HcSr04ReadDistanceInCentimeters(void)
{
		GPIOOn(pin_trigger);// pongo en alto el trigger
		DelayUs(PULSO_TRIGGER); // hago el delaay de 10 us
		GPIOOff(pin_trigger);// pongo en cero el trigger

		//true si esta en alto y false si tengo un cero
		while (GPIORead(pin_echo) == LOW){}
		while (GPIORead(pin_echo) == HIGH){

			DelayUs(PULSO_ECHO);
			cont_cm++;
		}

		distance_cm = cont_cm /38;

		cont_cm=0;

		return distance_cm;

}

uint16_t HcSr04ReadDistanceInInches(void)
{
			GPIOOn(pin_trigger);// pongo en alto el trigger
			DelayUs(PULSO_TRIGGER); // hago el delaay de 10 us
			GPIOOff(pin_trigger);// pongo en cero el trigger

	while (GPIORead(pin_echo) == LOW){}
	while (GPIORead(pin_echo) == HIGH)
	{
		DelayUs(PULSO_ECHO);
		cont_inches++;
	}

	distance_inches = cont_inches / 148;

	return distance_inches;

}

bool HcSr04Deinit()
{
	return true;
}
