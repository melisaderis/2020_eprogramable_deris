/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup hcsr04
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Deris Melisa
 *
 * This driver provide functions to use the distance sensor HCSR04
 *
 * @note FIUNER - 2do cuatrimestre 2020
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 5/09/2020  | Document creation		                         |

*/
/*#ifndef HC_SR4_H
#define HC_SR4_H*/

/*==================[inclusions]=============================================*/
#include "bool.h"
#include "gpio.h"

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief Initialization function of EDU-CIAA ports according to sensor's connections,
 * set direction and initial state of sensor ports
 * @param[in] echo gpio pin
 * @param[in] trigger gpio pin
 * @return true if no error occur */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/* * @brief obtaining the value of the distance in centimeters
 * @param[in] no parameter
 * @return a value that represents distance in centimeters */
uint16_t HcSr04ReadDistanceInCentimeters(void);

/* * @brief obtaining the value of the distance in inches
 * @param[in] no parameter
 * @return a value that represents distance in inches*/
uint16_t HcSr04ReadDistanceInInches(void);

/* * @brief Sensor initialization
 * @param[in] no parameter
 * @return true if no error occur*/
bool HcSr04Deinit(void);


/*#endif #ifndef HC_SR4_H */

