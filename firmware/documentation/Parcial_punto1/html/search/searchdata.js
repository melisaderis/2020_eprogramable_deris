var indexSectionsWithContent =
{
  0: "_abcdfghilmpstu",
  1: "adst",
  2: "g",
  3: "adghlstu",
  4: "bghimpt",
  5: "gls",
  6: "g",
  7: "abdghlstu",
  8: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules",
  8: "Pages"
};

