var annotated_dup =
[
    [ "analog_input_config", "structanalog__input__config.html", "structanalog__input__config" ],
    [ "digitalIO", "structdigital_i_o.html", "structdigital_i_o" ],
    [ "lcd_t", "structlcd__t.html", "structlcd__t" ],
    [ "lcdCustomChar_t", "structlcd_custom_char__t.html", "structlcd_custom_char__t" ],
    [ "serial_config", "structserial__config.html", "structserial__config" ],
    [ "timer_config", "structtimer__config.html", "structtimer__config" ]
];