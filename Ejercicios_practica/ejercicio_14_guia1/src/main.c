/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
 * Deris, Melisa
*/
/* CONSIGNA: Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros).
 */
/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
//#include <iostream>
 //using namespace std ??
/*==================[macros and definitions]=================================*/

typedef struct {
	char nombre [12];
	char apellido [20];
	uint32_t edad;
} alumno;


void cargar_datos (alumno *alumno2) {
	strcpy(alumno2 ->nombre, "Rocio");
	strcpy(alumno2 ->apellido, "Roda");
	alumno2 ->edad = 23;
}
/*==================[internal functions declaration]=========================*/
int main (void)
{
	alumno alumno1 = {"Melisa", "Deris", 26};
	printf("Mi nombre es:");
	printf("%s\r\n",alumno1.nombre);
	printf("Mi apellido es:");
	printf("%s\r\n",alumno1.apellido);
	printf("Mi edad es:");
	printf("%d",alumno1.edad);

	alumno alumno2;
	cargar_datos(&alumno2);
	printf(" El nombre de mi compañera es:");
	printf("%s\r\n",alumno2.nombre);
	printf(" El apellido de mi compañera es:");
	printf("%s\r\n",alumno2.apellido);
	printf(" La edad de mi compañera es:");
	printf("%d",alumno2.edad);

return 0;
}


/*==================[end of file]============================================*/

