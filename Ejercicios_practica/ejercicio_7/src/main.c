/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
 * Deris, Melisa
*/
/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
//#define mascara (1 << 3)
//#define mascara2 (1 << 13) | (1 <<14) no funciona para 32b
/*==================[internal functions declaration]=========================*/

int main(void)
{
	uint32_t variable;
	const uint32_t mascara =(1 << 3);
	printf ("%d\r\n", mascara);
	variable = variable | mascara; // 110 0001 1100 1101 0000 1000
	// mascara = (1 << 13) | ( 1 << 14); // error: assignment of read-only variable 'mascara' ES CONSTANTE
	const uint32_t mascara2 =(1 << 13) | (1 <<14);
	variable = variable & (~mascara2); // 110 0001 1000 1101 0000 1000
	printf("%d",variable); // 6409228 valor inicial
return 0;
}

/*==================[end of file]============================================*/

