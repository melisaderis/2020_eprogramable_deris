/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
  * Melisa Deris
Consigna: Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1. Luego,
mediante máscaras, coloque un 0 en el bit 4. */
/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void){

	// declaro un puntero que apunte a un entero cuyo valor es -1.
	int16_t valor = -1;
	int16_t *puntero;
	puntero = &valor; //siempre paso la dir de la variable
	const int16_t mascara = ~(1 << 4);
	*puntero = *puntero & mascara;
	printf("%d\r\n",*puntero);
return 0;
}





/*==================[end of file]============================================*/

