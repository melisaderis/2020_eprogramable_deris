/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
  * Melisa Deris
Consigna: Realice un programa que calcule el promedio de los 15 números listados abajo, para ello,
primero realice un diagrama de flujo similar al presentado en el ejercicio 9. (Puede utilizar la
aplicación Draw.io). Para la implementación, utilice el menor tamaño de datos posible: */
/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


/*==================[macros and definitions]=================================*/
#define cantidad 15
#define MODO_15 //correspondiente al vector de 15 valores

uint8_t valores []= {234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};

/*==================[internal functions declaration]=========================*/

/*uint8_t sumatoria(uint8_t vector[], uint cant_valores) {
	uint16_t suma = 0;
	int i = 0;
	for (i = 0; i < cant_valores; i++) {
		suma += vector[i];
	}
	printf("%d\n", suma);
	return (suma);
}*/

int main(void) {
	uint16_t promedio;

#ifndef MODO_15
	//uint8_t nuevo_valor= 233;
	// valores.push_back(nuevo_valor)
			valores[15]=233;
			//uint16_t suma = (sumatoria (&valores, 16));
			uint16_t suma = 0;
				int i = 0;
				for (i = 0; i < cantidad+1; i++) {
					suma += valores[i];
				}
			printf("%d\n", suma);
			promedio = suma>>4;

#else
		uint16_t suma = 0;
			int i = 0;
			for (i = 0; i < cantidad; i++) {
				suma += valores[i];}
					printf("%d\n", suma);
		promedio = suma/cantidad;
#endif
		printf("El promedio es:%d\n", promedio);
return 0;
}





/*==================[end of file]============================================*/

