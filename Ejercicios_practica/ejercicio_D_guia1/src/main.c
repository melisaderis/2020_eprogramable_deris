/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
  * Melisa Deris
Consigna: Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
typedef struct
{
//uint8_t port; !< GPIO port number
//uint8_t pin; !< GPIO pin number
//uint8_t dir; !< GPIO direction ‘0’ IN; ‘1’ OUT */
/*} gpioConf_t;
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14

La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.

 */
/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define OUT 1
#define IN 0
typedef struct
{
uint8_t port; /*!< GPIO port number */
uint8_t pin; /*!< GPIO pin number */
uint8_t dir; /*!< GPIO direction ‘0’ IN; ‘1’ OUT */
} gpioConf_t;

uint8_t puerto;
uint8_t pin;

gpioConf_t vector [4] = {
		{1,4,OUT},
		{1,5,OUT},
		{1,6,OUT},
		{2,14,OUT}
};
uint i;
void bcd_7seg (uint8_t digito, gpioConf_t *vector){

for(i=0; i<4;i++){
	puerto=vector[i].port;
	pin=vector[i].pin;
	if(digito&(1 << i)){
	printf("Está encendido el puerto %d",puerto);
	printf(".%d\n",pin);
	//printf("%d\n",digito);
	}
	else
	{
		printf("El puerto %d",puerto);
			printf(".%d",pin);
			printf(", está apagado \n");

	}

} //cierro la función
}
/*==================[internal functions declaration]=========================*/
int main (void){
uint8_t valor = 3;
bcd_7seg(valor, vector);
return 0;
}







/*==================[end of file]============================================*/

