/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
 * Melisa Deris
Consigna :
a. Declare una variable sin signo de 32 bits y cargue el valor 0x01020304. Declare
cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
cargue cada uno de los bytes de la variable de 32 bits.
b. Realice el mismo ejercicio, utilizando la definición de una “union” */

/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
uint32_t valor = 0x01020304;
uint8_t valor1;
uint8_t valor2;
uint8_t valor3;
uint8_t valor4;
int main(void)
{
valor1 = valor & (0xff);
printf("%d, es el valor de los primeros 8 bits.\r\n",valor1);
valor2 = (uint8_t) (valor >> 8);
printf("%d, es el valor de los segundos 8 bits.\r\n",valor2);
valor3 = (uint8_t) (valor >> 16);
printf("%d, es el valor de los terceros 8 bits.\r\n",valor3);
valor4 = (uint8_t) (valor >> 24);
printf("%d, es el valor de los cuartos 8 bits.\r\n",valor4);


// utilizando la función unión

union valor_32b {
	struct byte {
	uint8_t valor_1;
	uint8_t valor_2;
	uint8_t valor_3;
	uint8_t valor_4; } numero;
	uint32_t valor_total;
} valortotal;

valortotal.valor_total=0x01020304;
printf("%d, es el valor de los primeros 8 bits usando union.\r\n",valortotal.numero.valor_1);
printf("%d, es el valor de los segundos 8 bits usando union.\r\n",valortotal.numero.valor_2);
printf("%d, es el valor de los terceros 8 bits usando union.\r\n",valortotal.numero.valor_3);
printf("%d, es el valor de los cuartos 8 bits usando union.\r\n",valortotal.numero.valor_4);

return 0;
}

/*==================[end of file]============================================*/

