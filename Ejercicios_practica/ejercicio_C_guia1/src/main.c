/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
  * Melisa Deris
Consigna: Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida y un puntero a
un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD,
guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.
int8_t BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
 */
/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*valor1 = valor % 10; // me quedo con el resto de la división por 10.
valor = valor/10; //-> 123
valor2= valor%10; // -> 3
valor=valor/10; // -> 12, etc.*/
uint i;

int8_t BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number ){
	for(i=0; i<(digits); i++){
		bcd_number[i]= data%10;
		printf("%d Vector\r\n",bcd_number[i]);
		data=data/10;
		printf("%ddato \r\n ",data);
	}
// esto no sale invertido? es necesario invertirlo?
}
int main(void)
{
	uint32_t valor = 1487;
	uint8_t digitos= 4;
	uint8_t bcd_result[digitos];
BinaryToBcd(valor, digitos, bcd_result);
for(i=digitos; i>0; i--){
printf("%d",bcd_result[i-1]);
}
return 0;
}





/*==================[end of file]============================================*/

