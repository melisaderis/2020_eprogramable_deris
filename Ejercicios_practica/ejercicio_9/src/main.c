/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
 * Melisa Deris
Consigna : Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0.
Si es 0, cargue una variable “A” previamente declarada en 0, si es 1,
cargue “A” con 0xaa.

/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
	const uint32_t constante;
	const uint32_t mascara =(1 << 4);
	uint16_t a;
	if (constante & mascara == FALSE) {
		a=0;}
		else
			{a=0xaa;}
	printf("%d, es el valor de a\r\n",a);

//pruebo si funciona si variable tiene un 0 en el bit 4.
	const uint32_t constante1= 0x3400;
	uint16_t a1;
		if (constante1 & mascara== FALSE) {
			a1=0;}
			else
				{a1=0xaa;}
	printf("%d, es el valor de a para constante 1\r\n",a1);
	return 0;
}

/*==================[end of file]============================================*/

