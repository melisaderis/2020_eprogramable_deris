/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2020
 * Autora:
 * Melisa Deris
Consigna :
Realice un función que reciba un puntero a una estructura LED como la que se muestra a
continuación:
struct leds
{
uint8_t n_led; indica el número de led a controlar
uint8_t n_ciclos; indica la cantidad de ciclos de encendido/apagado
uint8_t periodo; indica el tiempo de cada ciclo
uint8_t mode; ON, OFF, TOGGLE
} my_leds;
 */

/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF  0
#define TOGGLE 2
/*==================[internal functions declaration]=========================*/
struct leds
{
uint8_t n_led; //indica el número de led a controlar
uint8_t n_ciclos; //indica la cantidad de ciclos de encendido/apagado
uint8_t periodo; //indica el tiempo de cada ciclo
uint8_t mode; //ON, OFF, TOGGLE
} my_leds;

uint i,j;
void manejar_leds(struct leds *struct_led) {
	switch (struct_led->mode) {
	case 0:
		switch (struct_led->n_led) {
		case 1:
			printf("Apagar el led 1 \n\r");
			break;
		case 2:
			printf("Apagar el led 2 \n\r");
			break;
		case 3:
			printf("Apagar el led 3 \n\r");
			break;

		} // cierra el switch de apagar

		break;
	case 1:
		switch (struct_led->n_led) {
		case 1:
			printf("Encender el led 1 \n\r");
			break;
		case 2:
			printf("Encender el led 2 \n\r");
			break;
		case 3:
			printf("Encender el led 3 \n\r");
			break;

		}	//cierro el switch de encender
break;
	case 2:
		for (j = 0; j < struct_led->n_ciclos; j++) {
			switch (struct_led->n_led) {
			case 1:
				printf("Toggle el led 1 \n\r");
				break;
			case 2:
				printf("Toggle el led 2 \n\r");
				break;
			case 3:
				printf("Toggle el led 3 \n\r");
				break;

			} //cierro el switch
			for (i = 0; i < struct_led->periodo; i++) {
				printf("Espera \n\r");
			} //cierro el for del retardo
		} //cierro el for de los ciclos

		break;
		} //cierra el switch de num de modo
	} //cierra la función


int main(void)
{
struct leds my_leds1 = {2,3,10,TOGGLE};
struct leds my_leds2 = {3,3,10,ON};
struct leds my_leds3 = {1,3,10,OFF};
manejar_leds(&my_leds1);
manejar_leds(&my_leds2);
manejar_leds(&my_leds3);
return 0;
}

/*==================[end of file]============================================*/

